""" Unit test for geo module """

import pytest
from floodsystem.station import MonitoringStation
from floodsystem.plot import plot_water_levels, plot_water_level_with_fit

def test_plot_water_levels():
    """ Check that plot does not accept inconsistent typical
        ranges or water_levels of value none """

    #Create a list of incosistent stations
    stations = get_test_station_list()
    
    #Assert that you get value errors
    assert plot_water_levels(stations[0], [1], [1]) == None
    assert plot_water_levels(stations[1], [1], [1]) == None

def test_plot_water_level_with_fit():
    """ Check that plot does not accept inconsistent typical
        ranges or water_levels of value none """

    #Create a list of incosistent stations
    stations = get_test_station_list()
    
    #Assert that you get value errors
    assert plot_water_level_with_fit(stations[0], [1], [1], 3) == None
    assert plot_water_level_with_fit(stations[1], [1], [1], 3) == None

def get_test_station_list():
    
     # Create a station
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "station1"
    coord = (-1.0, 1.0)
    trange = (7.0, 6.0)
    river = "River X"
    town = "Town1"
    s1 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    
    # Create a station
    s_id = "test-s-id2"
    m_id = "test-m-id2"
    label = "station2"
    coord = (-2.0, 3.0)
    trange = (-4.3, 3.8845)
    river = "River Y"
    town = "Town2"
    s2 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    return s1, s2
