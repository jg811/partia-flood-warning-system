# -*- coding: utf-8 -*-
"""
Created on Wed Jan 25 21:12:31 2017

@author: Arnav
"""

from floodsystem.geo import rivers_by_station_number
from floodsystem.stationdata import build_station_list

def run():
    """ Requirements for Task1E """

    #Build station list 
    stations = build_station_list()
    
    #Create a list of at least rivers containing the most stations
    z = rivers_by_station_number(stations,9)
    
    #Print the list of rivers
    print(z)

if __name__ == "__main__":
    print("*** Task 1E: CUED Part IA Flood Warning System ***")

    # Run Task1E
    run()