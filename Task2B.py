# -*- coding: utf-8 -*-
"""
Created on Fri Feb 17 15:55:26 2017

@author: Arnav
"""
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_level_over_threshold

def run():
    """ Requirements for Task2B """

    #Build station list
    stations1 = build_station_list()
        
    #Updates water levels of station 
    update_water_levels(stations1)
    
    y = stations_level_over_threshold(stations1, 0.8)
    for station in y:
        v = station[0]
        station_name = v.name
        station_level = station[1]
        print(station_name,station_level)

if __name__ == "__main__":
    print("*** Task 2B: CUED Part IA Flood Warning System ***")

    # Run Task2B
    run()