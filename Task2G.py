from floodsystem.warning import issue_flood_warnings
from floodsystem.stationdata import build_station_list, update_water_levels

def run():
    """ Requirements for Task2G """

    #Build list of stations
    stations = build_station_list()

    #Update water levels of station
    update_water_levels(stations)

    #Issue flood warning
    issue_flood_warnings(stations)

if __name__ == "__main__":
    print("*** Task2G: CUED Part IA Flood Warning System ***")

    #Run Task2G
    run()
