"""Unit test for the station module"""

import pytest
from floodsystem.station import MonitoringStation, inconsistent_typical_range_stations


def test_create_monitoring_station():

    # Create a station
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    typical_range = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s = MonitoringStation(s_id, m_id, label, coord, typical_range, river, town)

    assert s.station_id == s_id
    assert s.measure_id == m_id
    assert s.name == label
    assert s.coord == coord
    assert s.typical_range == typical_range
    assert s.river == river
    assert s.town == town
    
    return s

def test_inconsistent_typical_range_stations():
    
    #Create a station
    station = test_create_monitoring_station()
    
    #Create a list of inconsistent stations
    inconsistent_stations = inconsistent_typical_range_stations([station])
    
    #Assert that some station is in the list
    assert ("some station" in inconsistent_stations) == False
    
    