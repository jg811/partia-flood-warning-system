# CUED Part IA Flood Warning System Lab Group 33

This is the Part IA computing project at the Department of
Engineering, University of Cambridge.

The activity is documented at
http://cued-partia-flood-warning.readthedocs.io/.