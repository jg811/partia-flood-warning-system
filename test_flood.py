# -*- coding: utf-8 -*-
"""
Created on Sat Feb 25 13:25:06 2017

@author: Arnav
"""

import pytest
from floodsystem.station import MonitoringStation
from floodsystem.flood import stations_level_over_threshold, stations_highest_rel_level

def test_stations_level_over_threshold():
    
    list_of_over_threshold_stations=[]
    
    # Build list of stations
    stations = get_test_station_list()
    
    #Appends the latest_level of each station to 4.0
    for i in stations:
        i.latest_level = 4.0
    
    #Ccreates a list of stations which are over the threshold
    over_threshold = stations_level_over_threshold(stations, 0.8)
    
    #Loops through the list of stations which are over the threshold and appends the station name to a list
    for station in over_threshold:
        v = station[0]
        station_name = v.name
        list_of_over_threshold_stations.append(station_name)
    
    #Asserts that station1 is in the list of stations over the threshold but station3 is not
    assert ("station1" in list_of_over_threshold_stations) == True
    assert ("station3" in list_of_over_threshold_stations) == False
           
def test_stations_highest_rel_level():
    
    list_of_highest_rel_level = []
    
    # Build list of stations
    stations = get_test_station_list()
    
    #Appends the latest_level of each station to 4.0
    for i in stations:
        i.latest_level = 4.0
    
    #Creates a list of the N stations with the highest relative level when N=1    
    stations_with_highest_level = stations_highest_rel_level(stations,1)
    
    #Loops through the list of stations with the highest relative level and appends the station name to a list
    for station in stations_with_highest_level:
        v = station[0]
        station_name = v.name
        list_of_highest_rel_level.append(station_name)
    
    #Asserts that station2 is in the list of stations but station1 is not
    assert ("station2" in list_of_highest_rel_level) == True
    assert ("station1" in list_of_highest_rel_level) == False       
        
def get_test_station_list():
    
    # Create a station
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "station1"
    coord = (-1.0, 1.0)
    trange = (-7.0, 6.0)
    river = "River X"
    town = "Town1"
    latest_level = 4.0
    #relative water level = 0.846
    s1 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    
    # Create a station
    s_id = "test-s-id2"
    m_id = "test-m-id2"
    label = "station2"
    coord = (-2.0, 3.0)
    trange = (-4.3, 3.8845)
    river = "River Y"
    town = "Town2"
    latest_level = 4.0
    #relative water level = 1.014
    s2 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    
    # Create a station
    s_id = "test-s-id3"
    m_id = "test-m-id3"
    label = "station3"
    coord = (4.0, -6.0)
    trange = (4.3, 6.8845)
    river = "River Y"
    town = "Town3"
    latest_level = 5.0
    #relative water level = 0.271
    s3 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    
    return [s1,s2,s3]	
