# -*- coding: utf-8 -*-
"""
Created on Thu Jan 26 11:53:06 2017

@author: Arnav
"""

from floodsystem.stationdata import build_station_list
from floodsystem.station import inconsistent_typical_range_stations

def run():
    """ Requirements for Task1B """

    #Build station list
    stations = build_station_list()
 
    #Create a list of stations which have an inconsistent typical range
    a = inconsistent_typical_range_stations(stations)
    station_names = [station.name for station in a]
    #Print list of stations
    print (station_names)

if __name__ == "__main__":
    print("*** Task 1F: CUED Part IA Flood Warning System ***")

    # Run Task1F
    run()
