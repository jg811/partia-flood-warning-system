""" Unit test for geo module """

import pytest
from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_by_distance, stations_within_radius, rivers_with_station, stations_by_river, rivers_by_station_number
from floodsystem.station import MonitoringStation

def test_stations_by_distance():

    # Build list of stations
    stations = get_test_station_list()

    #Create a list by distance from coordinate
    stations_distance = stations_by_distance(stations, (0, 0))

    #Assert that station closest station and furthers station are in order and in a specific distance from coordinate
    assert stations_distance[0] == (stations[0], 157.24938127194397)
    assert stations_distance[-1] == (stations[-1], 801.3864856066193)

def test_stations_within_radius():
	
	#Build station list
	stations = get_test_station_list()
	
	#Create a list with stations within a radius of 400km of Origin
	stations_within_origin = stations_within_radius(stations, (0, 0), 400)

	#Assert that station Girton is in the list while Penberth is not
	assert (stations[0] in stations_within_origin) == True
	assert (stations[2] in stations_within_origin) == False

def test_rivers_with_station():
    
    #Build station list
    stations = get_test_station_list()
    
    #Create a list of rivers which have stations
    rivers_with_monitoring_stations = rivers_with_station(stations)
    
    #Assert that River X is in the list whilst River Z is not
    assert ("River X" in rivers_with_monitoring_stations) == True
    assert ("River Z" in rivers_with_monitoring_stations) == False

def test_stations_by_river():
    
    #Build station list
    stations = get_test_station_list()
    
    #Create a dictionary of rivers and their corresponding stations
    dictionary_of_rivers = stations_by_river(stations)
    
    #Assert that station1 is in the list of stations for River X but station2 is not
    assert ("station1" in dictionary_of_rivers["River X"]) == True
    assert ("station2" in dictionary_of_rivers["River X"]) == False

def test_rivers_by_station_number():
    
    #Build station list
    stations = get_test_station_list()
    
    #Create a list of tuples containing rivers and their corresponding number of stations
    river_names_and_number_of_stations = rivers_by_station_number(stations,1)
    
    #Assert that River Y is in the list and that the number of stations on River Y is 2
    assert ([("River Y",2)] == river_names_and_number_of_stations) == True
    
def get_test_station_list():
    
     # Create a station
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "station1"
    coord = (-1.0, 1.0)
    trange = (-7.0, 6.0)
    river = "River X"
    town = "Town1"
    s1 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    
    # Create a station
    s_id = "test-s-id2"
    m_id = "test-m-id2"
    label = "station2"
    coord = (-2.0, 3.0)
    trange = (-4.3, 3.8845)
    river = "River Y"
    town = "Town2"
    s2 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    
    # Create a station
    s_id = "test-s-id3"
    m_id = "test-m-id3"
    label = "station3"
    coord = (4.0, -6.0)
    trange = (4.3, 6.8845)
    river = "River Y"
    town = "Town3"
    s3 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    
    return [s1,s2,s3]	
