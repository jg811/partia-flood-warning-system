from floodsystem.stationdata import build_station_list 
from floodsystem.geo import stations_within_radius

def run():
	""" Requirements for Task1C """

	#Build station list
	stations = build_station_list()
	
	#Create a list with stations within a radius of 10km of Cambridge
	stations_within_cambridge = stations_within_radius(stations, (52.2053, 0.1218), 10)
	
	station_names = [s.name for s in stations_within_cambridge]
	#Print the stations within a radius of 10km of Cambridge
	print("Sations wihtin 10km of Cambridge: ")
	print(station_names)

if __name__ == "__main__":
    print("*** Task 1C: CUED Part IA Flood Warning System ***")

    # Run Task1C
    run()
