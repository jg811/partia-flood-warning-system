# -*- coding: utf-8 -*-
"""
Created on Mon Jan 23 22:13:26 2017

@author: Arnav
"""


from floodsystem.geo import rivers_with_station
from floodsystem.geo import stations_by_river
from floodsystem.stationdata import build_station_list

def run():
    """ Requirements for Task1B """
 
    #Build station list
    stations = build_station_list()
 
    #Create a list of rivers with stations sorted alphabetically
    x = rivers_with_station(stations)
     
    #Print first ten items in list
    print(x[:10])

    #Create a dictionary consisting of rivers and their corresponding stations sorted alphabetically
    y = stations_by_river(stations)
     
    #Create a variable River_Aire which is equal to the entry named "River Aire" in the dictionary
    River_Aire=y["River Aire"]
     
    #Create a variable River_Cam which is equal to the entry named "River Cam" in the dictionary
    River_Cam=y["River Cam"]

    #Create a variable Thames which is equal to the entry named "Thames" in the dictionary
    Thames=y["Thames"]

    #Print the contents of the variable River_Aire
    print(River_Aire)
    
    #Print the contents of the variable River_Cam
    print(River_Cam)
    
    #Print the contents of the variable Thames
    print(Thames)

if __name__ == "__main__":
    print("*** Task 1D: CUED Part IA Flood Warning System ***")

# Run Task1D
run()
  
