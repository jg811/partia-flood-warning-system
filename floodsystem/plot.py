""" This module contains a collection of functions related to
plotting data.

"""
import matplotlib.pyplot as plt
import matplotlib.dates as date
from .analysis import polyfit


def plot_water_levels(station, dates, levels):
    """ Display a plot of the water level data against
        against time for a station. It includes on the plot
        lines for the typical low and high levels

    """

    #Check for inconsitencies in station
    if station.typical_range_consistent() == False:
        return None
    if station.relative_water_level() == None:
        return None

    # Plot water level against time
    plt.plot(dates, levels)

    #Plot typical low and high levels
    plt.axhline(y=station.typical_range[0], label="lowest level")
    plt.axhline(y=station.typical_range[1], label="highest level")

    # Add axis labels, rotate date labels and add plot title
    plt.xlabel('date')
    plt.ylabel('water level (m)')
    plt.xticks(rotation=45);
    plt.title(station.name)

    # Display plot
    plt.tight_layout()  # This makes sure plot does not cut off date labels
    plt.show()

def plot_water_level_with_fit(station, dates, levels, p):
    """ Display a plot of the water level data against
    time for a station. Including plot lines for the
    typical low and highe levels and best fit polynomial
    of degree p

    """

    #Check for inconsitencies in station
    if station.typical_range_consistent() == False:
        return None
    if station.relative_water_level() == None:
        return None
    #Create best-fit polynomial for data
    poly, d0 = polyfit(dates, levels, p)

    # Plot polynomial fit at 100 points along interval
    x = date.date2num(dates)-d0

    #Plot water levels against dates and polynomial
    plt.plot(dates, levels, dates, poly(x))

    #Plot typical low and high levels
    plt.axhline(y=station.typical_range[0], label="lowest level")
    plt.axhline(y=station.typical_range[1], label="highest level")

    # Add axis labels, rotate date labels and add plot title
    plt.xlabel('date')
    plt.ylabel('water level (m)')
    plt.xticks(rotation=45);
    plt.title(station.name)

    # Display plot
    plt.tight_layout()  # This makes sure plot does not cut off date labels
    plt.show()
