"""This module contains a collection of functions related to
geographical data.

"""

from .utils import sorted_by_key
from haversine import haversine

def stations_by_distance(stations, p):
	""" Build a list of tuples containing the
	distance of a station from the coordinate p,
	sorted by distance, using the Haversine library

	 """
	
	#Create a list to store the distances
	distance_from_coordinate = []	
	
	#Calculate the distance from the stations to the point by iterating over the list of stations in Km
	for station in stations:
		distance_from_coordinate += [(station, float(haversine(station.coord, p)))]

	#Sort the tuples by using utils.sort_by_key	
	distance_sorted = sorted_by_key(distance_from_coordinate, 1)

	return distance_sorted

def stations_within_radius(stations, centre, r):
	""" Build a list of all stations within radius r
	of a geographic coordinate by using stations_by_distance
	and then only returning those with distance smaler than or equal
	to r. 

	"""

	#Create a list to store the stations within the radius
	stations_in_circle = []

	#Use sort_by_distance to create a list of stations and their distance from the centre sorted by closest to furthest
	distance_from_centre = stations_by_distance(stations, centre)
	

	#Iterate of the list of tuples and include those who are within radius distance
	for station, distance in distance_from_centre:
		if distance <= r:
			stations_in_circle += [station]
		else:
			break #as they are ordered from closest to furthest this will make the function faster	
		
	return sorted(stations_in_circle, key = lambda x: x.name)

def rivers_with_station(stations):
    """Build a list of rivers which have at least one station and sort alphabetically"""
    
    #Create a list to store the rivers which have stations in
    list_of_rivers=[]

    #Iterate through all the stations and add each river which has a station in it
    for j in stations:
        if j.river not in list_of_rivers:
            list_of_rivers.append(j.river)
    
    #Sort the list of rivers alphabetically
    list_of_rivers = sorted(list_of_rivers)
    
    return list_of_rivers
    
def stations_by_river(stations):
    """Build a dictionary containing all the rivers and a list of stations on them"""
    
    #Create a dictionary to store the rivers and their corresponding stations in
    river_names = {}

    #Create a list of all the rivers in the data
    list_of_all_rivers = set([e.river for e in stations])

    #Iterate through all the rivers and add each station which is located on it to a list
    for i in list_of_all_rivers:
        list_of_stations=[]
        for station in stations:
            if station.river == i:
                list_of_stations.append(station.name)
        river_names[i]=sorted(list_of_stations) #Sort the list of stations and enter as a new entry in the dictionary

    return river_names

def rivers_by_station_number(stations, N):
    """Build a list of tuples containing river names and their number of stations. Output
    the top N rivers in descending order"""
    
    #Create a list of river names and their corresponding number of stations
    river_names_and_number_of_stations = []

    #Create a list of all the rivers in the data
    list_of_all_rivers = set([e.river for e in stations])
    
    #Iterate through all the rivers and add each station which is located on it to a list. 
    for l in list_of_all_rivers:
        list_of_stations=[]   
        for station in stations:
            if station.river == l:
                list_of_stations.append(station.name)
        tuplevalue = (l,len(list_of_stations))
        river_names_and_number_of_stations.append(tuplevalue) #Create a tuple containing the river name and the length of the aforementioned list
        river_names_and_number_of_stations = sorted_by_key(river_names_and_number_of_stations,1,True) #Order the list of tuples according to the number of stations for each river
    
    #Iterate through list of tuples to ensure that rivers with the same number of stations are included in the list
    for z in range (N,len(river_names_and_number_of_stations)): 
        if river_names_and_number_of_stations[z][1]==river_names_and_number_of_stations[z-1][1]: 
           N += 1
        else:
            break
        
    return river_names_and_number_of_stations[:N]

