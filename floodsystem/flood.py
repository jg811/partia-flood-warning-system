"""This module provides an interface for checking the likelihood
of a flood and subsequently warning of it"""

from floodsystem.station import MonitoringStation
from .utils import sorted_by_key

def stations_level_over_threshold(stations,tol):
    
    #Create an empty list in which the stations over the tolerance will be stored
    stations_over_tolerance = []

    #Ensures the data is valid for each station and appends its name and water_level as a tuple into a list
    for station in stations:
        if station.typical_range_consistent() == True:
            water_level = station.relative_water_level()
            if type(water_level) == float:
                if water_level > tol:
                    stations_over_tolerance.append((station, water_level))
    
    #Sorts the stations over tolerance by descending order of water level
    stations_over_tolerance = sorted_by_key(stations_over_tolerance, 1,reverse=True)
    
    return stations_over_tolerance
            
def stations_highest_rel_level(stations, N):
   
   #Creates an empty list which will contain the water levels ordered in descending order 
   ordered_water_levels = []
   
   #Ensures the data is valid for each station and appends its name and water_level as a tuple into a list
   for station in stations:
        if station.typical_range_consistent()==True:
            water_level = station.relative_water_level()
            if water_level != None:
                ordered_water_levels.append((station, water_level))
   
   #Sorts the stations by descebding order of water level
   ordered_water_levels = sorted_by_key(ordered_water_levels, 1,reverse=True)
   
   #Slices the list so only the first N items are outputted
   at_risk_stations = ordered_water_levels[:N]
   
   return at_risk_stations
        
