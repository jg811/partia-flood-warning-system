"""This module provides a model for a monitoring station, and tools
for manipulating/modifying station data

"""


class MonitoringStation:
    """This class represents a river level monitoring station"""

    def __init__(self, station_id, measure_id, label, coord, typical_range,
                 river, town):

        self.station_id = station_id
        self.measure_id = measure_id

        # Handle case of erroneous data where data system returns
        # '[label, label]' rather than 'label'
        self.name = label
        if isinstance(label, list):
            self.name = label[0]

        self.coord = coord
        self.typical_range = typical_range
        self.river = river
        self.town = town

        self.latest_level = None

    def __repr__(self):
        d = "Station name:     {}\n".format(self.name)
        d += "   id:            {}\n".format(self.station_id)
        d += " measure id: {}\n".format(self.measure_id)
        d += "   coordinate:    {}\n".format(self.coord)
        d += "   town:          {}\n".format(self.town)
        d += "   river:         {}\n".format(self.river)
        d += "   typical range: {}".format(self.typical_range)
        return d
    
    def typical_range_consistent(self):
        """Checks that the typical_range is not empty or inconsistent"""
        
        result = self.typical_range
        
        #Checks that the typical_range data is not missing
        if type(result) != tuple: 
            return False
        else:
            if self.typical_range[1]<self.typical_range[0]: #Checks that the lowest level is not greater than the highest level 
                return False
            else:
                return True
    
    def relative_water_level(self):
        """Returns the latest water level as a fraction of the typical range"""
        
        
        #Checks if the necessary data is available and if so returns the latest water level as a fraction of the typical range
        if type(self.latest_level) == float:
            if (self.typical_range_consistent()==True):
                fraction_of_typical_range = (self.latest_level-self.typical_range[0])/((self.typical_range)[1]-(self.typical_range[0]))
                return fraction_of_typical_range
        else:
            return None
        

def inconsistent_typical_range_stations(stations):
    """Creates a list of stations which have inconsistent data for the typical range of stations"""
    
    #Creates a list of inconsistent stations
    list_of_inconsistent_stations = []

    #Iterates through all stations adding the inconsistent ones to the list
    for m in stations:
        if m.typical_range_consistent() == False:
            list_of_inconsistent_stations.append(m)
    
    #Sorts the list of stations alphabetically
    list_of_inconsistent_stations = sorted(list_of_inconsistent_stations, key = lambda x: x.name)
    return list_of_inconsistent_stations
            
