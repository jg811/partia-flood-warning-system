
""" This module contains a collection of functions related to
issuing flood warnings, by analysisng numerical data".

"""
import numpy as np
import matplotlib.dates
from .flood import stations_level_over_threshold
from .analysis import flood_coeff

def issue_flood_warnings(stations, tol=0.8, p=4, dt=2):
    """ Issues 4 different types of flood risks, low, moderate, high,
    and severe depending on the flooding coefficient

    """

    #Create list for the 4 different types of floodings
    low_risk = []
    moderate_risk = []
    high_risk = []
    severe_risk = []

    #Create a list of stations with levels higher than tolerance
    stations_above_tol = stations_level_over_threshold(stations, tol)

    #Create a lis of flood coefficients for the stations with valid data
    stations_flood_coeff = flood_coeff([station for station, level in stations_above_tol], p, dt)

    #Issue warnings
    for station, coeff in stations_flood_coeff:
        rel_water_lvl = station.relative_water_level()
        if (coeff >= 10) and (rel_water_lvl>=2):
            severe_risk.append((station.name, rel_water_lvl ))
        elif coeff >= 2 and rel_water_lvl >= 1.2 or rel_water_lvl>=2:
           high_risk.append((station.name, rel_water_lvl))
        elif coeff >= 1 and (rel_water_lvl >= 1):
            moderate_risk.append((station.name, rel_water_lvl))
        elif (coeff >= 0.8):
            low_risk.append((station.name, rel_water_lvl))
    #Print the list for 
    print("Towns in low risk of flooding:")
    print(low_risk)
    print("Towns in moderate risk of flooding:")
    print(moderate_risk)
    print("Towns in high risk of flooding:")
    print(high_risk)
    print("Towns in severe risk of flooding:")
    print(severe_risk)
