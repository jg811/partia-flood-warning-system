""" This module contains a collection of functions related to
analysing data, to creat best fit functions for data.

"""
import numpy as np
import matplotlib.dates
import datetime
from .datafetcher import fetch_measure_levels

def polyfit(dates, levels, p):
    """ Computes a least squares fit of polynomial
    of degree p to water level data.

    """
    #Check for inconsitencies
    if dates != []:

        #Change list of dates into list of floats
        dates_float = matplotlib.dates.date2num(dates)

        #Shift of date(time) axis
        d0 = dates_float[0]

        # Find coefficients of best-fit polynomial f(x) of degree p
        p_coeff = np.polyfit(dates_float-d0, levels, p)

        # Convert coefficient into a polynomial that can be evaluated,
        poly = np.poly1d(p_coeff)

        return poly, d0

def gradient_polyfit_last_date(dates, levels, p):
    """ Computes the gradient of the polyfit
    function at the latest date.

    """

    #Check for inconsitencies
    if dates != []:

        #Get polynomial coefficients and time shift
        poly, d0 = polyfit(dates, levels, p)

        #Create an array to store the coefficients of the gradient line
        g_coeff= [poly.c[0]*(p-i) for i in range(p)]

        #Convert coefficients inot a polynomial that can be evaluated
        gradient = np.poly1d(g_coeff)

        #Latest dateime in float and shifted time
        return gradient(matplotlib.dates.date2num(dates[-1])-d0)

def flood_coeff(stations, p, dt):
    """ Computes the flooding coeefficient, for a
    stations, from the gradient of the curve at the
    latests data point relative to a number of day dt
    and the current relative water level.

    """

    #Create a list to store flood coefficients
    flood_coeff = []

    #Multiply the gradient tiem the relative water of the station for each station
    for station in stations:
        zip_dates = []
        zip_levels = []
        dates, levels = fetch_measure_levels(station.measure_id, dt=datetime.timedelta(days=dt))
        for date, level in zip(dates, levels):
            zip_dates.append(date)
            zip_levels.append(level)
        if zip_dates != []:
            gradient = np.power(2, (gradient_polyfit_last_date(zip_dates, zip_levels, p)-1)/10)
        if (gradient != None) and (station.relative_water_level() != None):
            flood_coeff.append((station, gradient*station.relative_water_level()))
    return flood_coeff
