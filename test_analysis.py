""" Unit test for geo module """

import pytest
import datetime
import numpy as np
from datetime import datetime, timedelta
from floodsystem.analysis import polyfit, gradient_polyfit_last_date
import logging
import sys

def test_polyfit(capsys):
    """Check that polyfit returns the correct polynomial coefficients and time shift"""

    #Test data
    t = [datetime(2016, 12, 30), datetime(2016, 12, 31), datetime(2017, 1, 1), datetime(2017, 1, 2), datetime(2017, 1, 3), datetime(2017, 1, 4), datetime(2017, 1, 5)]
    level = [0.2, 0.7, 0.95, 0.92, 1.02, 0.91, 0.64]
    p = 2

    #Check the output of polyfit
    assert polyfit(t, level, p)[1] == 736328.0
    print(polyfit(t, level, p)[0])
    printoutput = capsys.readouterr()
    assert printoutput == ('          2\n-0.06417 x + 0.4496 x + 0.2481\n', '')

def test_gradient_polyfit_(capsys):
    """Check that polyfit returns the correct polynomial coefficients and time shift"""

    #Test data
    t = [datetime(2016, 12, 30), datetime(2016, 12, 31), datetime(2017, 1, 1), datetime(2017, 1, 2), datetime(2017, 1, 3), datetime(2017, 1, 4), datetime(2017, 1, 5)]
    level = [0.2, 0.7, 0.95, 0.92, 1.02, 0.91, 0.64]
    p = 2

    #Check the output of polyfit
    assert gradient_polyfit_last_date(t, level, p) == -0.8341666666666665

