# -*- coding: utf-8 -*-
"""
Created on Mon Feb 20 17:40:59 2017

@author: Arnav
"""

from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_highest_rel_level

def run():
    """ Requirements for Task2C """

    #Build station list
    stations1 = build_station_list()
    
    #Updates water levels of station 
    update_water_levels(stations1)
    
    l = stations_highest_rel_level(stations1, 10)
    for station in l:
        v = station[0]
        station_name = v.name
        station_level = station[1]
        print(station_name,station_level)


if __name__ == "__main__":
    print("*** Task 2C: CUED Part IA Flood Warning System ***")

    # Run Task2C
    run()