from floodsystem.stationdata import build_station_list 
from floodsystem.geo import stations_by_distance

def run():
	""" Requirements for Task1B """

	#Build station list
	stations = build_station_list()
	
	#Create a list with the distance from stations to Cambridge
	distance_from_cambridge = stations_by_distance(stations, (52.2053, 0.1218))
	
	#Create a list to output (station name, town, distance) 
	stations_from_cambridge = []	

	#Use a for loop to create the list above
	for station, distance in distance_from_cambridge:
		stations_from_cambridge += [(station.name, station.town, distance)] 
	
	#Print the the closest 10 entries
	print("Closest stations from Cambridge: ")
	print(stations_from_cambridge[:10])

	#Print the furtherst 10 entries
	print("Furthest stations from Cambridge: ")
	print(stations_from_cambridge[-10:])

if __name__ == "__main__":
    print("*** Task 1B: CUED Part IA Flood Warning System ***")

    # Run Task1B
    run()
