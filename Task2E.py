import datetime
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.plot import plot_water_levels
from floodsystem.flood import stations_highest_rel_level
from floodsystem.stationdata import build_station_list, update_water_levels

def run():
    """ Requirements for Task2E """

    #Build list of stations
    stations = build_station_list()

    #Update water levels of station
    update_water_levels(stations)

    #Fetch the 5 stations at which the current relative water lavel is greatest
    stations_level_greatest = stations_highest_rel_level(stations, 5)

    #Plot the water levels for past 10 days for the above stations
    dt = 10
    for station in stations_level_greatest:
        zip_dates = []
        zip_levels = []
        dates, levels = fetch_measure_levels(station.measure_id, dt=datetime.timedelta(days=dt))
        for date, level in zip(dates, levels):
            zip_dates.append(date)
            zip_levels.append(level)
        plot_water_levels(station, zip_dates, zip_levels)

if __name__ == "__main__":
    print("*** Task 2E: CUED Part IA Flood Warning System ***")

    #Run Task2E
    run()
